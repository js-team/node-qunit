# Installation
> `npm install --save @types/qunit`

# Summary
This package contains type definitions for QUnit (http://qunitjs.com/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/qunit.

### Additional Details
 * Last updated: Sun, 19 Dec 2021 05:01:23 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [James Bracy](https://github.com/waratuman), [Mike North](https://github.com/mike-north), [Stefan Sechelmann](https://github.com/sechel), [Chris Krycho](https://github.com/chriskrycho), [Dan Freeman](https://github.com/dfreeman), and [James C. Davis](https://github.com/jamescdavis).
